import mongoose from "mongoose"

let connectToDb = async()=>{
    try {
        await mongoose.connect("mongodb://0.0.0.0:27017/WebuserManagement")
        console.log("our data base is successfully connected");
    } catch (error) {
        console.log(error.message);
    }
}
export default connectToDb