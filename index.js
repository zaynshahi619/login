import express, { json } from "express"
import connectToDb from "./database/ConnectToDb.js";
import webUserRouter from "./router/webUserRouter.js";



let expressApp = express()
expressApp.use(json())
let port = 8000
expressApp.listen(port,()=>{
    console.log(`our app is listening to port ${port}`);
})

connectToDb()

expressApp.use("/web-users",webUserRouter)



