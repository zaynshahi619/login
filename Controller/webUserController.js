import { secretKey } from "../config.js"
import { WebUser } from "../model/model.js"
import bcrypt from "bcrypt"
import { sendMail } from "../utils/sendMail.js"
import jwt from "jsonwebtoken"

export let webUserCreate = async(req,res,next)=>{
    try {
        let data = req.body
        let hashPassword = await bcrypt.hash(data.password,10)
        data={
            ...data,
            isVerifiedEmail:false,
            password:hashPassword
        }

        let result = await WebUser.create(data)

        let infoObj = {
            id: result._id
        }
        let expiryInfo={
            expiresIn:"5d"
        }
        let token = await jwt.sign(infoObj,secretKey,expiryInfo)

        await sendMail({
      from:'"Unique" <zaynshahi619@gmail.com>',
      to:[data.email],
      subject: "verify-email",
      html: `<p>Click below to verify your email</p>
            <a href="http://localhost:8000/verify-email?token=${token}">http://localhost:8000/verify-email?token=${token}</a>
      `
    })



        res.json({
            success:true,
            message:"create successful",
            result:result

        })
    } catch (error) {
        res.json({
            success:false,
            message:error.message
        })
    }
}

export let verifyEmail = async(req,res,next)=>{
    try {
        let tokenString = req.headers.authorization
        let token = tokenString.split(" ")[1]
        let infoObj = await jwt.verify(token,secretKey)
        let userId = infoObj.id
        let result = await WebUser.findByIdAndUpdate(userId,{isVerifiedEmail:true},{new:true})
        res.json({
            success:true,
            message:"isVerified email updated successfully"
        })
    } catch (error) {
        res.json({
            success:false,
            message:error.message
        })
    }
}

export let loginUser = async(req,res,next)=>{
    try {
        let email = req.body.email
        let password = req.body.password
        let user = await WebUser.findOne({email:email})
        if(user){
            if(user.isVerifiedEmail){
                let isValidPassword = await bcrypt.compare(password,user.password)
                if(isValidPassword){
                    let infoObj = {
                        id:user.id
                    }
                    let expiryInfo = {
                        expiresIn:"365d"
                    }
                    let token = await jwt.sign(infoObj,secretKey,expiryInfo)

                    res.json({
                        success:true,
                        message:"login successful",
                        token:token
                    })
                }
                else{
                    res.json({
                        success:false,
                        message:"email or password does not match"
                    })
                }
            }
            else{
                res.json({
                    success:false,
                    message:"email or password does not match"
                })
            }

        }
        else{
            res.json({
                success:false,
                message:"email or password does not match"
            })
        }
    } catch (error) {
        res.json({
            success:false,
            message:error.message
        })
    }
}

export let myProfile = async(req,res,next)=>{
    try {
        let id = req.id
        let result = await WebUser.findById(id)
        res.json({
            success:true,
            message:"my profile read successfully",
            result:result
        })
    } catch (error) {
        res.json({
            success:false,
            message:error.message
        })
    }
}

export let updateProfile = async(req,res,next)=>{
    try {
        let id = req.id
        let data = req.body
        delete data.email
        delete data.password

        let result = await WebUser.findByIdAndUpdate(id,data,{new:true})
        res.json({
            success:true,
            message:"Profile updated successfully",
            result:result
        })
    } catch (error) {
        res.json({
            success:false,
            message:error.message
        })
    }
}

export let updatePassword = async(req,res,next)=>{
    try {
        let id = req.id
        let oldPassword = req.body.oldPassword
        let newPassword = req.body.newPassword

        let data = await WebUser.findById(id)

        let hashPassword = data.password
        
        let isValidPassword = await bcrypt.compare(oldPassword,hashPassword)
        if(isValidPassword){
            let newHashPassword = await bcrypt.hash(newPassword,10)

            let result = await WebUser.findByIdAndUpdate(id,{password:newHashPassword},{new:true})
            res.json({
                success:true,
                message:"Password updated successfully",
                result:result
            })
        }
        else{
            res.json({
                success:false,
                message:error.message,
            })
        }

    } catch (error) {
        res.json({
            success:false,
            message:error.message
        })
    }
}

export let readAllUser = async(req,res,next)=>{
    try {
        let result = await WebUser.find({})
        res.json({
            success:true,
            message:"Read all user successfully",
            result:result
        })
    } catch (error) {
        res.json({
            success:false,
            message:error.message
        })
    }
}

export let readSpecificUser = async(req,res,next)=>{
    try {
        let id = req.params.id
        let result = await WebUser.findById(id)
        res.json({
            success:true,
            message:"Read all user successfully",
            result:result
        })
    } catch (error) {
        res.json({
            success:false,
            message:error.message
        })
    }
}

export let updateSpecificUser = async(req,res,next)=>{
    try {
        let id = req.params.id
        let data = req.body
        delete data.email
        delete data.password
        let result = await WebUser.findByIdAndUpdate(id,data,{new:true})
        res.json({
            success:true,
            message:"Read all user successfully",
            result:result
        })
    } catch (error) {
        res.json({
            success:false,
            message:error.message
        })
    }
}

export let deleteSpecificUser = async(req,res,next)=>{
    try {
        let id = req.params.id
        let result = await WebUser.findByIdAndDelete(id)
        res.json({
            success:true,
            message:"Read all user successfully",
            result:result
        })
    } catch (error) {
        res.json({
            success:false,
            message:error.message
        })
    }
}

export let forgotPassword = async(req,res,next)=>{
    try {
        let email = req.body.email
        let result = await WebUser.findOne({email:email})
        if(result){
            let infoObj = {
                id: result._id
            }
            let expiryInfo={
                expiresIn:"365d"
            }
            let token = await jwt.sign(infoObj,secretKey,expiryInfo)

            await sendMail({
                from:'"Unique" <zaynshahi619@gmail.com>',
                to:email,
                subject: "reset-password",
                html: `<p>Click the link below to reset password</p>
                      <a href="http://localhost:8000/reset-password?token=${token}">http://localhost:8000/reset-password?token=${token}</a>
                `
              })
              res.json({
                success:true,
                message:"Link has been sent to reset your password"
              })

        }

    } catch (error) {
        res.json({
            success:false,
            message:error.message
        })
    }
}

export let resetPassword = async(req,res,next)=>{
    try {
        let id = req.id
        let hashPassword = await bcrypt.hash(req.body.password,10)

        let result = await WebUser.findByIdAndUpdate(id,{password:hashPassword},{new:true})

        res.status(201).json({
            success:true,
            message:"password reset successfully",
            result:result
        })
    } catch (error) {
        res.status(400).json({
            success:false,
            message:error.message,
           
        })
    }
}