import jwt from "jsonwebtoken"
import { secretKey } from "../config.js";

let isAuthenticated = async(req,res,next)=>{
    try {
        let tokenString = req.headers.authorization
        let token = tokenString.split(" ")[1]
        let user = await jwt.verify(token,secretKey)
        req.id = user.id
        next()
    } catch (error) {
        res.json({
            success:false,
            message:error.message
        })
    }
}
export default isAuthenticated